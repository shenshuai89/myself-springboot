package com.learn.mybtsp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MybtspApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybtspApplication.class, args);
    }

}
