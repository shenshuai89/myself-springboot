package com.learn.mybtsp.controller;

import com.learn.mybtsp.pojo.User;
import com.learn.mybtsp.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("user")
@Api(value = "用户相关API",tags = "用户管理相关的接口")
public class UserController {
    @Autowired
    private UserService userService;

    @ApiImplicitParam(name = "查询全部用户", value = "查询全部用户")
    //说明是什么方法(可以理解为方法注释)
    @ApiOperation(value = "查询全部用户", notes = "查询全部用户")
    @GetMapping("all")
    public List<User> findAll(){
        List<User> all = userService.findAll();
        return all;
    }

    // 为了测试admin和user权限区别
    @GetMapping("admin/all")
    public List<User> findAdminAll(){
        List<User> all = userService.findAll();
        return all;
    }

    @GetMapping("page")
    public List<User> findPage(@RequestParam("page") int page,
                               @RequestParam("size") int size){
        List<User> page1 = userService.findPage(page, size);
        return page1;
    }

    @GetMapping("findAge")
    public List<User> findAge(@RequestParam("age") int age){
        return  userService.findAge(age);
    }

    //方法参数说明，name参数名；value参数说明，备注；dataType参数类型；required 是否必传；defaultValue 默认值
    @ApiImplicitParam(name = "add user", value = "新增用户数据")
    //说明是什么方法(可以理解为方法注释)
    @ApiOperation(value = "添加用户", notes = "添加用户")
    @GetMapping("save")
    public Long save(@RequestParam("name") String name){

        return userService.save(name);
    }

    @GetMapping("send")
    public String send(){
        userService.send();
        return "success";
    }
}
