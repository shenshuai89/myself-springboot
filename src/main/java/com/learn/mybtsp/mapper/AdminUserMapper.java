package com.learn.mybtsp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.learn.mybtsp.pojo.AdminUser;

public interface AdminUserMapper extends BaseMapper<AdminUser> {
}