package com.learn.mybtsp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.learn.mybtsp.pojo.Role;

import java.util.List;

// 创建role角色
public interface RoleMapper extends BaseMapper<Role> {
    List<Role> findRoleListByUserId(Long userId);
}
