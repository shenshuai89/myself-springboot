package com.learn.mybtsp.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.learn.mybtsp.pojo.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    //查询大于age的user列表
    List<User> findUserGtAge(int age);
}
