package com.learn.mybtsp.pojo;

import lombok.Data;

// 创建Permission权限类
@Data
public class Permission {
    private Integer id;

    private String name;

    private String desc;

    private String permissionKeyword;

    private String path;
}