package com.learn.mybtsp.pojo;

import lombok.Data;

// 创建role角色类
@Data
public class Role {
    private Integer id;

    private String roleName;

    private String roleDesc;

    private String roleKeyword;
}