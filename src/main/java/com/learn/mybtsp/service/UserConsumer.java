package com.learn.mybtsp.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@RocketMQMessageListener(topic = "topic_springboot", consumerGroup = "group1")
public class UserConsumer implements RocketMQListener<String> {
    @Override
    public void onMessage(String msg) {
        log.info("message: {}",msg);
    }
}
