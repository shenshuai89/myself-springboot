package com.learn.mybtsp.service;

import com.learn.mybtsp.domain.repository.ArticleRepository;
import com.learn.mybtsp.model.es.Article;
import com.learn.mybtsp.model.es.Author;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.Arrays;

import static java.util.Arrays.asList;


@SpringBootTest
public class ESTest {
    @Autowired
    private ArticleRepository articleRepository;

    @Test
    public void testSave(){
        Article article1 = new Article("springboot union Elasticsearch");
        article1.setAuthors(asList(new Author("god"), new Author("John")));
        articleRepository.save(article1);

        article1 = new Article("springboot union Elasticsearch222");
        article1.setAuthors(asList(new Author("god"), new Author("King")));
        articleRepository.save(article1);

        article1 = new Article("springboot union Elasticsearch333");
        article1.setAuthors(asList(new Author("god"), new Author("Bill")));
        articleRepository.save(article1);
    }

    @Test
    public void queryAuthorName(){
        Page<Article> articles = articleRepository.findByAuthorsName("god", PageRequest.of(0,10));
        for(Article article : articles.getContent()){
            System.out.println(article);
            for(Author author : article.getAuthors()){
                System.out.println(author);
            }
        }
    }

    @Test
    public void update(){
        Page<Article> articles = articleRepository.findByTitle("springboot union Elasticsearch",PageRequest.of(0,10));
        Article article = articles.getContent().get(0);
        System.out.println(article);
        System.out.println(article.getAuthors().get(0));
        Author author = new Author("god");
        article.setAuthors(Arrays.asList(author));
        articleRepository.save(article);
    }

    @Test
    public void delete(){
        Page<Article> articles = articleRepository.findByTitle("springboot union Elasticsearch",PageRequest.of(0,10));

        Article article = articles.getContent().get(0);
        articleRepository.delete(article);
    }
}
